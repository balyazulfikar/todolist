import React from 'react';
import {render} from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css'; //supaya bisa dipakai semua komponen

render(<App />,document.querySelector('#root')); //kode ini membuat dom pada browser kita
