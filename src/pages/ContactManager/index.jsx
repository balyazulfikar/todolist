import React, {useState, useEffect} from 'react'
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import api from "./api/contact"
import ContactList from './ContactList'

export default function ContactManagerPage() {
    const [contacts, setContacts] = useState([])

    const retriveContacts = async () => {
      const response = await api.get("/contacts")
      return response.data
    }

    useEffect(() => {
      const getAllContacts = async () => {
        const allContacts = await retriveContacts();
        if(allContacts) setContacts(allContacts)
      }

      getAllContacts();
    }, []);

    console.log(contacts);


    return (
      <div className="ui container">
       <Router>
         <Switch>
           <Route path="/"
           exact
           render={(props) => (
             <ContactList
             {...props}
             contacts={contacts}
            />
           )}
           />
         </Switch>
       </Router>
      </div>
    )
  }

