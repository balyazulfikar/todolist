import React from 'react'
import ContactCard from './ContactCard'

export default function ContactList(props) {
  console.log(props.contacts)

  const renderedContactList = props.contacts.map((contact => {
    return(
      <ContactCard 
        contact={contact}
        key={contact.id}
      />
    )
  }))
  return (
    <div className="main">
      <h2>Contact List</h2>
      <div className="ui celled list">{renderedContactList}</div>
    </div>
  )
}
