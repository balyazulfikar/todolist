import React from 'react'
import { Link } from 'react-router-dom'
import user from './images/user.png'


export default function ContactCard(props) {
    const {id, name, email} = props.contact
    
    return (
      <div>
        <div className="item">
          <img src={user} alt="user/" className="ui avatar image" />
          <div className="content">
            <Link to={{pathname: `contact/${id}`, state:{contact:props.contact}}}>
              <div className="header">{name}</div>
              <div>{email}</div>
            </Link>
          </div>
        </div>
      </div>
    )
  }


