import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import api from "./TodoApi/Todo";
import TodoList from "./TodoList";
import "./TodoPage.css";
import AddTodo from "./TodoAdd";
import { uuid } from "uuidv4";
import "./TodoPage.css";

export default function TodoPage() {
  const [todos, setTodos] = useState([]);

  //retrive
  const retriveTodos = async () => {
    const response = await api.get("/todoList");
    setTodos(response.data);
  };

  //add contact
  const addHandlerContact = async (todo) => {
    const request = {
      id: uuid(),
      ...todo,
    };

    const response = await api.post("/todoList", request);
    setTodos([...todos, response.data]);
  };

  //remove contact
  const removeHandlerContact = async (id) => {
    await api.delete(`/todoList/${id}`);
    const newContactListAfterDeleteWithSpecificID = todos.filter((todo) => {
      return todo.id !== id; //jika contact.id tidak sama dgn id yg disebut, maka dia akan tersimpan lagi
    });

    setTodos(newContactListAfterDeleteWithSpecificID);
  };

  useEffect(() => {
    retriveTodos();
  }, []);

  // console.log(todo)

  return (
    <div className="ui container">
      <Router>
        <Switch>
          <Route path="/" exact render={(props) => <TodoList {...props} todos={todos} getContactRemoveID={removeHandlerContact} />} />

          {/* addTodo */}
          <Route path="/add" render={(props) => <AddTodo {...props} addContactHandler={addHandlerContact} />} />
        </Switch>
      </Router>
    </div>
  );
}
