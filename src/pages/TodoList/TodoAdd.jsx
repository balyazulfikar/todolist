import React, { Component } from "react";

class AddTodo extends Component {
  state = {
    day: "",
    activity: "",
  };

  add = (e) => {
    e.preventDefault();
    //jika salah satu kolom kosong, maka akan keluar alert
    if ((this.state.day === "") | (this.state.activity === "")) {
      alert("kolom kosong, tolong diisi terlebih dahulu");
      return;
    }
    //jika sudah terisi semua
    //proses addContactHandler ==> yg akan menyambunkan ke API post method
    this.props.addContactHandler(this.state);
    this.setState({ day: "", activity: "" });
    this.props.history.push("/"); //render ke routernya
  };

  render() {
    return (
      <div className="ui main">
        <h2>Add Contact</h2>
        <form className="ui form" onSubmit={this.add}>
          <div className="field">
            <label>day</label>
            <input type="text" day="day" placeholder="day" value={this.state.day} onChange={(e) => this.setState({ day: e.target.value })} />
          </div>
          <div className="field">
            <label>activity</label>
            <input type="activity" day="activity" placeholder="activity" value={this.state.activity} onChange={(e) => this.setState({ activity: e.target.value })} />
          </div>
          <div>
            <button className="ui button blue">Add</button>
          </div>
        </form>
      </div>
    );
  }
}

export default AddTodo;
