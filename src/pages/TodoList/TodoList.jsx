import React from "react";
import TodoCard from "./TodoCard";
import { Link } from "react-router-dom";

export default function TodoList(props) {
  console.log(props.todos);

  const deleteContact = (id) => {
    props.getContactRemoveID(id);
  };

  const renderedTodoList = props.todos.map((todo) => {
    return <TodoCard todo={todo} clickHandler={deleteContact} key={todo.id} />;
  });
  return (
    <div className="main">
      <h2>Todo list</h2>
      <Link to="/add">
        <button className="ui button blue right">Add activity</button>
      </Link>
      <div className="ui celled list">{renderedTodoList}</div>
    </div>
  );
}
