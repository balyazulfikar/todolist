import React from "react";

export default function TodoCard(props) {
  const { id, day, activity } = props.todo;

  return (
    <div>
      <div className="item">
        <div className="content">
          <h3 className="header">{day}</h3>
          <div>{activity}</div>
          <i className="trash alternate icon" onClick={() => props.clickHandler(id)}></i>
        </div>
      </div>
    </div>
  );
}
