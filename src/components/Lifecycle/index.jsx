import React, { Component } from 'react'

class ClassComponentsLifecycle extends Component {
  constructor(){
    super();
    //Step 1
    this.state = {
      // data: [{"body":"Data larik 1"}, {"body":"Data Larik 2"}]
      //declare struktur data yg mau disimpan
      data : [] //struktur data berupa object array > supaya bisa di filter, maping dll
    }

    // this.loadData = this.loadData.bind(this) //untuk memasukkan fungsi ke construct
  }

  //componentDidMount
  componentDidMount(){
    this.loadData()
    this.tambahData()
  }

  tambahData(){
    this.setState({
      data: this.state.data.concat([{"body":"Data Larik 3"}, {"body":"Data Larik 4"}, {"body":"Data larik 5"}])
    })
  }

  //step 2 = Buat fungsi untuk mendapatkan data/ menembak API
  loadData(){
    return fetch("http://jsonplaceholder.typicode.com/posts")
    .then(res =>{
      if(!res.ok) throw Error(res.statusText)
      return res.json() //dibaca dengan fungsi json()
    })
    .then(resjson => {
      this.setState({
        data: resjson //di ganti semuanya /replace data
      });
    })
  }

  // render
  render() {
    console.log(this.state.data) //data ada?
    const DataPosts = this.state.data //disimpan di sebuah variable, agar muda pemanggilannya
    
    return (
    <div>
      <h1>List Data</h1>
      {/* {JSON.stringify(DataUsers)} */}
      <ol>
        {/* { DataPosts.filter((item, index) => index < 10)
        .map(item => 
            <li key={item.id}>{item.title}</li>
        )} */}
        {DataPosts.map((item, index) => (
          <li key={index}>{item.body}</li>
        ))}
      </ol>
    </div>
    )
  }
}

export default ClassComponentsLifecycle
