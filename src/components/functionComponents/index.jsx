import React, { useState, useEffect } from 'react'
import { Button } from '../ButtonComponent'
import axios from 'axios'

export const FunctionComponent = () => {
  const [nama, setNama] = useState("default name")
  /**
   * state = this.state
   * setState() = this.setState()
   * 
   * initialState
   * this.state = {
   *   initialState = 0;
   * }
   */
  const changeName = () => {
    setNama("FE kelas Siang")
  }

  return (
    <div>
      <h1>{nama}</h1>
      <Button title={"ubah nama"} action={changeName}/>
    </div>
  )
}


//fungsi untuk fetch API
export const GetDataFromAPI = () => {
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);

  //langsung DidMount
  useEffect(() => {
    // getData("http://jsonplaceholder.typicode.com/users")
    // getData("http://jsonplaceholder.typicode.com/posts")
    getData("https://newsapi.org/v2/everything?q=Apple&from=2021-07-15&sortBy=popularity&apiKey=ee878d000ad54d149f46d3ee11ea392c")
  }, [])

  const getData = async (url) => {
    try {
      const a = await axios.get(url); //untuk nembak ke url nya, terus get response disimpan dalam variable res
      const dataAPI = await a.data; //hasil dari res di convert menjadi data lalu disimpan sebagai variable dataAPI
      // setUsers(dataAPI)
      setPosts(dataAPI.articles) //init setState
    } catch (error) {
      console.log(error) //error handling
    }
  }

  console.log(posts)
  return (
    <div>
      <h1>List Data:</h1>
        <ol>
          {posts.map((item, idx) => (
            <li key={idx}>
              {item.title}
              <a href={item.url} target="_blank" rel="noopener noreferrer"> Get Link</a>
            </li>
          ))}
        </ol> 
    </div>
  )
}



